`pyenv` is a python version manager. It allows you to install, activate, and set a system default for python versions like python 2.7, python 3.9, etc.

1. Install `pyenv`:
```sh
curl https://pyenv.run | bash
```

2. Add the following to your `~/.bashrc` or `~/.zshrc`
```sh
eval "$(pyenv init -)"

# for lazy loading:
pyenv() { # load pyenv on first pyenv invocation
   unset -f pyenv
   eval "$(pyenv init -)"
}
```

3. Restart your shell
```sh
exec $SHELL
```

4. Confirm pyenv is installed by running it
```sh
pyenv

# should see output like
pyenv 1.2.21
Usage: pyenv <command> [<args>]

Some useful pyenv commands are:
   --version   Display the version of pyenv
   activate    Activate virtual environment
   commands    List all available pyenv command...
```

5. Install python version desired
```sh
# list all available versions
pyenv install -l

# search for versions
pyenv install -l | grep 2.7

# install it
pyenv install 2.7.18
```

6. Set your python version
```sh
pyenv global 2.7.18
```

7. Confirm your python + pip versions are correct
```sh
python --version
# outputs:
Python 3.8.5 # NOTE: I previously had run `pyenv global 3.8.5`

pip --version
# outputs:
pip 20.1.1 from /Users/leo/.pyenv/versions/3.8.5/lib/python3.8/site-packages/pip (python 3.8)

which python
# outputs:
python is /Users/leo/.pyenv/shims/python
python is /usr/bin/python

which pip
# outputs:
pip is /Users/leo/.pyenv/shims/pip
```